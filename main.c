#include <stdio.h>
#include <stdlib.h>

typedef struct node
{
    int data;
    struct node *next;
    struct node *prev;
} node;

typedef struct
{
    node *tail;
    node *head;
    int count;
} queue;

node *pop(queue *q);
void push(queue *q, int data);
queue *newQueue();

void printQueue(queue *q);

void main()
{
    queue *q = newQueue();
    int secim, data;
    node *nodeData;
    while (1)
    {
        printf("1. Kuyruga eleman ekleme\n2. Kuyruktan eleman alma\n3. Kuyruktaki tum elemanlari goster\n4. Cikis\n");
        scanf("%d", &secim);
        switch (secim)
        {
        case 1:
            printf("Eklemek istediginiz veriyi giriniz: ");
            scanf("%d", &data);
            push(q, data);
            break;
        case 2:
            nodeData = pop(q);
            if (nodeData != NULL)
                printf("Alinan eleman: %d\n", nodeData->data);
            else
                printf("Kuyrukta hic eleman bulunmamaktadır.\n");
            break;
        case 3:
            printQueue(q);
            break;
        case 4:
            return;
            break;
        default:
            break;
        }
    }
}

node *pop(queue *q)
{
    if (q->count == 0)
    {
        return NULL;
    }
    else
    {
        node *data = q->head;
        q->head = q->head->next;
        q->count--;
        if (q->count == 0)
        {
            q->tail = NULL;
            return data;
        }
        q->head->prev = NULL;
        return data;
    }
}

void push(queue *q, int data)
{
    node *newNode = (node *)malloc(sizeof(node));
    newNode->next = NULL;
    newNode->prev = NULL;
    newNode->data = data;
    if (q->count == 0)
    {
        q->tail = q->head = newNode;
    }
    else
    {
        q->tail->next = newNode;
        newNode->prev = q->tail;
        q->tail = newNode;
    }
    q->count++;
}

queue *newQueue()
{
    queue *q = (queue *)malloc(sizeof(queue));
    q->tail = NULL;
    q->head = NULL;
    q->count = 0;
}

void printQueue(queue *q)
{
    if (q->count == 0)
    {
        printf("Kuyrukta hic eleman bulunmamaktadir.\n");
        return;
    }
    while (q->count > 0)
    {
        printf("%d", pop(q)->data);
        if (q->count > 0)
        {
            printf(" -> ");
        }
    }
    printf("\n");
}